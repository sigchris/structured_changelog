require 'structured_changelog/roadmap_section'

class StructuredChangelog
  RSpec.describe RoadmapSection do
    subject { described_class.new(section_text) }

    context "## ROADMAP x.y.z" do
      let(:section_text) { <<~SECTION }
        ## ROADMAP #{version}

        * something
        * something else
      SECTION

      let(:version) { "1.2.3" }

      describe "#version" do
        it "is equal to the section version" do
          expect(subject.version).to eq Gem::Version.new(version)
        end
      end
    end

    context "## ROADMAP x.y" do
      let(:section_text) { <<~SECTION }
        ## ROADMAP #{version}

        * something
        * something else
      SECTION

      let(:version) { "1.2" }

      describe "#version" do
        it "is equal to the section version" do
          expect(subject.version).to eq Gem::Version.new(version)
        end
      end
    end

    context "## ROADMAP" do
      let(:section_text) { <<~SECTION }
        ## ROADMAP

        * something
        * something else
      SECTION

      describe "#version" do
        it "is empty" do
          expect(subject.version).to eq Gem::Version.new("0")
        end
      end
    end

    context "## NEXT RELEASE" do
      let(:section_text) { <<~SECTION }
        ## NEXT RELEASE

        * something
        * something else
      SECTION

      describe "#version" do
        it 'is empty' do
          expect(subject.version).to eq Gem::Version.new("0")
        end
      end
    end
  end
end
