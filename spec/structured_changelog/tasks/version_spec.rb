require 'structured_changelog/tasks/version'

RSpec.describe 'changelog:version', type: :rake do
  let(:task_options) { {path: Pathname.new(__FILE__)/'..'/'..'/'..'/'CHANGELOG.md'} }

  it 'prints the current version according to the changelog' do
    expect { execute_task }.to output("0.10.0\n").to_stdout
  end
end
