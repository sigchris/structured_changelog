require 'structured_changelog/tasks/recent'

RSpec.describe 'changelog:recent', type: :rake do
  let(:task_options) { { path: Pathname.new(__FILE__)/'..'/'..'/'..'/'CHANGELOG.md' } }

  it 'displays the n most recent release sections' do
    expect { execute_task }.to output(/0\.10\.0.*0\.2\.0.*0\.1\.1/m).to_stdout
  end
end
