require 'structured_changelog/tasks/preview'

RSpec.describe 'changelog:preview', type: :rake do
  let(:task_options) { { repo_path: repo_path, changelog_path: changelog_path } }

  let(:repo_path)      { Pathname.new(Dir.mktmpdir) }
  let(:changelog_path) { repo_path/'CHANGELOG.md' }

  let(:repo) { Git.init(repo_path.to_s) }

  let(:previous_section) { "## RELEASE 1.0.0\n\n* BREAKING: it's so much better now (right? please let it actually be better oh no it isn't is it)" }

  before do
    changelog_path.write(previous_section)

    repo.add(all: true)
    repo.commit("first commit")
    repo.add_tag("v1.0.0")
  end

  after { repo_path.rmtree }

  subject(:preview) { `rake changelog:preview\[#{repo_path},#{changelog_path}\]`.chomp }

  context "with changelog commits since the last release" do
    let(:fix_lines)   { "* FIX: we fixed something\n* FIX: we fixed something else" }
    let(:fix_message) { "here is a description\n\n#{fix_lines}" }

    let(:feature_line)    { "* FEATURE: we added something" }
    let(:feature_message) { "here is a description\n\nand a line\n#{feature_line}\nand another line" }

    before do
      repo.commit(fix_message, allow_empty: true)
      repo.commit(feature_message, allow_empty: true)
    end

    it 'displays a release section with all the changelog notes' do
      expect { safely_execute_task }.to output(<<~OUTPUT).to_stdout_from_any_process
        ## RELEASE 1.1.0

        #{fix_lines}
        #{feature_line}
      OUTPUT
    end
  end

  context "with no changelog commits since the last release" do
    it "errors and says no release notes since the last release" do
      expect { safely_execute_task }
        .to output("No release notes since the last release\n")
        .to_stderr_from_any_process

      expect($?.exitstatus).to eq 1
    end
  end
end
