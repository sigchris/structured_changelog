require 'structured_changelog/tasks/sync'

RSpec.describe 'changelog:sync', type: :rake do
  let(:task_options) do
    {
      changelog_path: Pathname.new(__FILE__)/'..'/'..'/'..'/'CHANGELOG.md',
      version_path:   Pathname.new(__FILE__)/'..'/'..'/'..'/'version.rb',
    }
  end

  before do
    Pathname.new(task_options[:version_path]).write <<~FILE
      module FakeGem
        VERSION = "0.9.0"
      end
    FILE
  end

  after { File.delete(task_options[:version_path]) }

  it "sets the gem's version to the current changelog version" do
    execute_task

    require task_options[:version_path]

    changelog_version = StructuredChangelog.new(task_options[:changelog_path]).version.to_s
    gem_version       = FakeGem::VERSION

    expect(gem_version).to eq changelog_version
  end
end
