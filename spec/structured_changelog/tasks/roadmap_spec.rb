require 'tmpdir'

require 'structured_changelog/tasks/roadmap'

RSpec.describe "changelog:roadmap", type: :rake do
  let(:task_options) do
    { repo_path: Pathname.new(Dir.mktmpdir) }.tap do |options|
      options[:changelog_path] = options[:repo_path]/'CHANGELOG.md'
    end
  end

  context "with roadmap sections in the CHANGELOG.md" do
    let(:roadmap_sections) do
      <<~ROADMAP
        ## ROADMAP 3.0.0

        * they're going to love this

        ## ROADMAP 2.1.0

        * something something
        * something else

        ## ROADMAP 2.0.0

        * another thing
      ROADMAP
    end

    before { task_options[:changelog_path].write(roadmap_sections) }
    after  { task_options[:repo_path].rmtree }

    it "outputs the roadmap sections" do
      expect { execute_task }.to output(roadmap_sections).to_stdout
    end
  end
end
