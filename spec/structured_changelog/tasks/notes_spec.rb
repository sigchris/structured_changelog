require 'structured_changelog/tasks/notes'

RSpec.describe 'changelog:notes', type: :rake do
  let(:task_options) { { path: path, query: query } }

  let(:path) { Pathname.new(__FILE__)/'..'/'..'/'..'/'CHANGELOG.md' }

  let(:zero_one_zero) { "## RELEASE 0.1.0\n\n* FEATURE: thingy\n" }
  let(:zero_one_one)  { "## RELEASE 0.1.1\n\n* FIX: red\n" }
  let(:zero_two_zero) { "## RELEASE 0.2.0\n\n* FEATURE: blue\n" }
  let(:zero_ten_zero) { "## RELEASE 0.10.0\n\n* FEATURE: sorting\n" }

  let(:expected_output) { release_sections.join("\n") }

  context "by default" do
    let(:task_options) { { path: path } }

    let(:release_sections) { [ zero_ten_zero ] }

    it 'displays notes for the current version' do
      expect { execute_task }.to output(expected_output).to_stdout
    end
  end

  context "current release" do
    let(:query)            { "current" }
    let(:release_sections) { [ zero_ten_zero ] }

    it 'displays notes for the current version' do
      expect { execute_task }.to output(expected_output).to_stdout
    end
  end

  context "every release" do
    let(:query)            { "all" }
    let(:release_sections) { [ zero_ten_zero, zero_two_zero, zero_one_one, zero_one_zero ] }

    it "displays the notes for all the releases" do
      expect { execute_task }.to output(expected_output).to_stdout
    end
  end

  context "single release" do
    let(:query)            { "0.1.0" }
    let(:release_sections) { [ zero_one_zero ] }

    it 'displays notes for just that release' do
      expect { execute_task }.to output(expected_output).to_stdout
    end
  end

  context "releases after a version" do
    let(:query)            { "0.1.1 <" }
    let(:release_sections) { [ zero_ten_zero, zero_two_zero, zero_one_one ] }

    it 'displays the notes for 0.1.0, 0.1.1, 0.2.0' do
      expect { execute_task }.to output(expected_output).to_stdout
    end
  end

  context "releases before a version" do
    let(:query)            { "< 0.1.1" }
    let(:release_sections) { [ zero_one_one, zero_one_zero ] }

    it 'displays the notes for 0.1.0, 0.1.1, 0.2.0' do
      expect { execute_task }.to output(expected_output).to_stdout
    end
  end

  context "releases between two versions" do
    let(:query)            { "0.1.0 < 0.2.0" }
    let(:release_sections) { [ zero_two_zero, zero_one_one, zero_one_zero ] }

    it 'displays the notes for 0.1.0, 0.1.1, 0.2.0' do
      expect { execute_task }.to output(expected_output).to_stdout
    end
  end
end
