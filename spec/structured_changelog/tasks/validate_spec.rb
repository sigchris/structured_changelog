require 'structured_changelog/tasks/validate'

RSpec.describe 'changelog:validate', type: :rake do
  context "with a valid CHANGELOG.md" do
    let(:task_options) { { path: Pathname.new(__FILE__)/'..'/'..'/'..'/'CHANGELOG.md' } }

    it 'validates a good changelog' do
      expect { execute_task }.to output(/Valid /).to_stdout
    end
  end

  context "with an invalid changelog" do
    let(:task_options) { { path: Pathname.new(__FILE__)/'..'/'..'/'..'/'BADLOG.md' } }

    it "doesn't validate a bad changelog" do
      expect { execute_task }.to output(/No RELEASE blocks/).to_stdout
    end
  end
end
