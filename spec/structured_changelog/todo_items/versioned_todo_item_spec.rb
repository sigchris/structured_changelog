require 'structured_changelog/todo_items/versioned_todo_item'

class StructuredChangelog
  module TodoItems
    RSpec.describe VersionedTodoItem do
      describe '::appropriate_for_comment?' do
        context "a patch todo comment" do
          it "returns false" do
            expect(described_class).to_not be_appropriate_for_comment("# PATCH TODO: something")
          end
        end

        context "a minor todo comment" do
          it "returns false" do
            expect(described_class).to_not be_appropriate_for_comment("# MINOR TODO: something")
          end
        end

        context "a major todo comment" do
          it "returns false" do
            expect(described_class).to_not be_appropriate_for_comment("# MAJOR TODO: something")
          end
        end

        context "a version todo comment with 3 version components" do
          it 'returns true' do
            expect(described_class).to be_appropriate_for_comment("# 3.0.0 TODO: something")
          end
        end

        context "a version todo comment with 4+ version components" do
          it "returns true" do
            expect(described_class).to be_appropriate_for_comment("# 3.0.0.4-rc2 TODO: something")
          end
        end

        context "a version todo comment with only 2 version components" do
          it "returns true" do
            expect(described_class).to be_appropriate_for_comment("# 3.2 TODO: something")
          end
        end

        context "a version todo comment with a single version component" do
          it "returns false" do
            expect(described_class).to_not be_appropriate_for_comment("# 3 TODO: something")
          end
        end
      end

      describe '#completion_version_from_version' do
        subject { described_class.from_comment("# 3.0.1 TODO: something") }

        let(:current_version) { Gem::Version.new("2.0.0") }

        it 'returns the version specified by the comment' do
          expect(subject.completion_version_from_version(current_version))
            .to eq Gem::Version.new("3.0.1")
        end
      end
    end
  end
end
