require 'structured_changelog/todo_items/major_todo_item'

class StructuredChangelog
  module TodoItems
    RSpec.describe MajorTodoItem do
      describe '::appropriate_for_comment?' do
        context "a patch todo comment" do
          it "returns patch" do
            expect(described_class).to_not be_appropriate_for_comment("# PATCH TODO: something")
          end
        end

        context "a minor todo comment" do
          it "returns false" do
            expect(described_class).to_not be_appropriate_for_comment("# MINOR TODO: something")
          end
        end

        context "a major todo comment" do
          it "returns false" do
            expect(described_class).to be_appropriate_for_comment("# MAJOR TODO: something")
          end
        end

        context "a version todo comment" do
          it "returns false" do
            expect(described_class).to_not be_appropriate_for_comment("# 3.0.0 TODO: something")
          end
        end
      end

      describe '#completion_version_from_version' do
        subject { described_class.from_comment("# MAJOR TODO: something") }

        let(:current_version) { Gem::Version.new("2.0.0") }

        it 'returns the version specified by the comment' do
          expect(subject.completion_version_from_version(current_version))
            .to eq Gem::Version.new("3.0.0")
        end
      end
    end
  end
end
