require 'structured_changelog/release_preview'

class StructuredChangelog
  RSpec.describe ReleasePreview do
    subject { described_class.new(deps: deps) }

    let(:deps) do
      DependencyBundle.new(
        changelog:            changelog,
        repo:                 instance_double("Git::Base"),
        release_line_fetcher: release_line_fetcher,
      )
    end

    let(:changelog)            { instance_double("StructuredChangelog") }
    let(:release_line_fetcher) { instance_double("StructuredChangelog::ReleaseLineFetcher") }

    describe "#empty?" do
      before do
        allow(changelog).to receive(:version).and_return(Gem::Version.new("0.1.0"))
        allow(release_line_fetcher).to receive(:release_lines_since_tag)
          .and_return(release_lines)
      end

      context "when there are no commits since the last release" do
        let(:release_lines) { [] }

        it "is true" do
          expect(subject).to be_empty
        end
      end

      context "when there are release lines since the last release" do
        let(:release_lines) { [ "* FIX: Fixed a thing" ] }

        it "is false" do
          expect(subject).to_not be_empty
        end
      end
    end

    describe "#new_version" do
      before do
        allow(changelog).to receive(:version).and_return(Gem::Version.new("0.1.0"))
        allow(release_line_fetcher).to receive(:release_lines_since_tag)
          .and_return(release_lines)
      end

      let(:fix_message)         { "* FIX: fix" }
      let(:enhancement_message) { "* ENHANCEMENT: enhancement" }
      let(:deprecation_message) { "* DEPRECATION: deprecation" }
      let(:feature_message)     { "* FEATURE: feature" }
      let(:breaking_message)    { "* BREAKING: breaking" }

      context "when the sharpest change is a FIX change" do
        let(:release_lines) { [ fix_message ] }

        it "is a patch bump greater than the current version" do
          expect(subject.new_version).to eq changelog.version.bump_patch
        end
      end

      context "when the sharpest change is a ENHANCEMENT change" do
        let(:release_lines) { [ enhancement_message ] }

        it "is a patch bump greater than the current version" do
          expect(subject.new_version).to eq changelog.version.bump_patch
        end
      end

      context "when the sharpest change is a DEPRECATION change" do
        let(:release_lines) { [ deprecation_message ] }

        it "is a patch bump greater than the current version" do
          expect(subject.new_version).to eq changelog.version.bump_patch
        end
      end

      context "when the sharpest change is a FEATURE change" do
        let(:release_lines) do
          [ feature_message, fix_message, enhancement_message, deprecation_message ]
        end

        it "is a patch bump greater than the current version" do
          expect(subject.new_version).to eq changelog.version.bump_minor
        end
      end

      context "when the sharpest change is a BREAKING change" do
        let(:release_lines) do
          [ breaking_message, feature_message, fix_message, enhancement_message, deprecation_message ]
        end

        it "is a patch bump greater than the current version" do
          expect(subject.new_version).to eq changelog.version.bump_major
        end
      end
    end

    describe "#to_s" do
      before do
        allow(changelog).to receive(:version).and_return(Gem::Version.new("0.1.0"))
        allow(release_line_fetcher).to receive(:release_lines_since_tag)
          .and_return(release_lines)
      end

      context "when there are release lines since the last release" do
        let(:release_lines) do
          [
            "* FIX: Fixed a thing",
            "* FEATURE: Added a thing",
          ]
        end

        it "contains each release message" do
          expect(subject.to_s).to include "* FIX: Fixed a thing"
          expect(subject.to_s).to include "* FEATURE: Added a thing"
        end

        it 'contains the new version' do
          expect(subject.to_s).to include "RELEASE 0.2.0"
        end
      end
    end
  end
end
