require 'structured_changelog/release_line_fetcher'

class StructuredChangelog
  RSpec.describe ReleaseLineFetcher do
    subject { described_class.new(deps: deps) }

    let(:deps) { DependencyBundle.new }

    let(:tmpdir)           { Pathname.new(Dir.mktmpdir) }
    let(:repo)             { Git.init(tmpdir.to_s) }
    let(:changelog_path)   { tmpdir/'CHANGELOG.md' }
    let(:previous_section) { "## RELEASE 1.0.0\n\n* BREAKING: it's so much better now (right? please let it actually be better oh no it isn't is it)" }
    let(:tag)              { "v1.0.0" }

    before do
      deps.set(repo: repo)

      changelog_path.write(previous_section)

      repo.add(all: true)
      repo.commit("first commit")
      repo.add_tag(tag)
    end

    after do
      tmpdir.rmtree
    end

    describe "#release_lines_since_tag" do
      before do
        commit_messages.each { |commit_message| repo.commit(commit_message, allow_empty: true) }
      end

      context "with commits with release lines since the last tag" do
        let(:release_lines) do
          described_class::RELEASE_LINE_PREFIXES.map { |prefix| "* #{prefix}: message" }
        end
        let(:commit_messages) do
          [
            "some other commit",
            *release_lines.map { |line| "subject\n\n#{line}" },
          ]
        end

        it 'returns the release lines since the last tag' do
          expect(subject.release_lines_since_tag(tag)).to eq release_lines
        end
      end

      context "without commits with release lines since the last tag" do
        let(:commit_messages) { [] }

        it "returns an empty array" do
          expect(subject.release_lines_since_tag(tag)).to eq []
        end
      end
    end
  end
end
