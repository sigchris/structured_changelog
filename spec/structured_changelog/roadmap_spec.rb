require 'structured_changelog/roadmap'
require 'structured_changelog/todo_items'

class StructuredChangelog
  RSpec.describe Roadmap do
    let(:deps) { DependencyBundle.new(changelog: instance_double("StructuredChangelog")) }

    subject { described_class.new(deps: deps) }

    before do
      allow(deps.changelog).to receive(:version).and_return(Gem::Version.new("2.1.3"))
    end

    describe "#add_todo_item" do
      let(:todo) { "something something" }

      before { subject.add_todo_item(todo_item) }

      context "with a versioned todo item" do
        let(:todo_item) { TodoItems::VersionedTodoItem.new(todo: todo, version: version) }
        let(:version)   { Gem::Version.new("4.0.0") }

        it 'lists the todo item under version 4.0.0' do
          expect(subject.todo_item_lists_by_completion_version[version]).to eq [todo_item]
        end
      end

      context "with a major todo item" do
        let(:todo_item) { TodoItems::MajorTodoItem.new(todo: todo) }
        let(:version)   { Gem::Version.new("3.0.0") }

        it 'lists the todo item under version 3.0.0' do
          expect(subject.todo_item_lists_by_completion_version[version]).to eq [todo_item]
        end
      end

      context "with a minor todo item" do
        let(:todo_item) { TodoItems::MinorTodoItem.new(todo: todo) }
        let(:version)   { Gem::Version.new("2.2.0") }

        it 'lists the todo item under version 2.2.0' do
          expect(subject.todo_item_lists_by_completion_version[version]).to eq [todo_item]
        end
      end

      context "with a patch todo item" do
        let(:todo_item) { TodoItems::PatchTodoItem.new(todo: todo) }
        let(:version)   { Gem::Version.new("2.1.4") }

        it 'lists the todo item under version 2.1.4' do
          expect(subject.todo_item_lists_by_completion_version[version]).to eq [todo_item]
        end
      end
    end
  end
end
