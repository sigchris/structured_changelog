require 'tmpdir'

require 'git'
require 'fantaskspec'
require 'dependency_bundle'

require 'rspec_config'
require 'support/rake_spec_methods'

require 'coveralls'
Coveralls.wear!

require 'structured_changelog'


