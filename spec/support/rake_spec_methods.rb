module RakeSpecMethods
  def safely_execute_task
    task_options = respond_to?(:task_options) ? self.task_options : {}

    Process.waitpid Process.fork { task.execute(task_options) }
  end

  def execute_task
    task_options = respond_to?(:task_options) ? self.task_options : {}

    task.execute(task_options)
  end
end

RSpec.configure do |config|
  config.include RakeSpecMethods, type: :rake
end
