require 'pathname'
require 'set'
require 'structured_changelog/release_section'
require 'structured_changelog/roadmap_section'
require 'structured_changelog/release_section_filters'

class StructuredChangelog
  attr_reader :path, :release_sections, :roadmap_sections

  def initialize(path)
    @path = Pathname.new(path)

    @release_sections = Set.new([])
    @roadmap_sections = Set.new([])

    parse
  end

  def version
    latest_release_section.version
  end

  def validate
    notifications = []

    if latest_release_section.nil?
      notifications << "No RELEASE blocks"
    else
      notifications += latest_release_section.validate
    end

    notifications.each(&method(:puts))

    notifications.empty?
  end

  def find_release_sections(query)
    ReleaseSectionFilters
      .filter_for(query)
      .filter_release_sections(release_sections)
  end

  private

  def latest_release_section
    release_sections.max
  end

  def parse
    capture = []

    [*path.readlines, :EOF].each do |line|
      if line == :EOF || RoadmapSection.start_with?(line) || ReleaseSection.start_with?(line)
        if RoadmapSection.start_with?(capture.first)
          roadmap_sections << RoadmapSection.new(capture.join)
          capture = []
        elsif ReleaseSection.start_with?(capture.first)
          release_sections << ReleaseSection.new(capture.join)
          capture = []
        end
      end

      capture << line
    end

    self
  end
end
