require 'git'
require 'dependency_bundle'

require 'structured_changelog'
require 'structured_changelog/release_preview'
require 'structured_changelog/release_line_fetcher'

desc "View the release section changelog:compile would insert into the changelog without actually doing so"
task 'changelog:preview', [:repo_path, :changelog_path] do |_task, arguments|
  repo_path      = arguments.to_h.fetch(:repo_path)      { Pathname.pwd }
  changelog_path = arguments.to_h.fetch(:changelog_path) { Pathname.pwd/"CHANGELOG.md" }

  deps = DependencyBundle.new(
    changelog: StructuredChangelog.new(changelog_path),
    repo:      Git.open(repo_path),
  )
  deps.set(release_line_fetcher: StructuredChangelog::ReleaseLineFetcher.new(deps: deps))

  release_preview = StructuredChangelog::ReleasePreview.new(deps: deps)

  abort("No release notes since the last release") if release_preview.empty?

  puts release_preview
end
