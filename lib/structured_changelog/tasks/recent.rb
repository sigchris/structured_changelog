require 'structured_changelog'

desc 'Display N Recent Release Sections'
task 'changelog:recent', [:n, :path] do |_task, arguments|
  n    = arguments.to_h.fetch(:n)    { 3 }
  path = arguments.to_h.fetch(:path) { 'CHANGELOG.md' }

  changelog        = StructuredChangelog.new(path)
  release_sections = changelog.release_sections.to_a[0...n]

  puts release_sections.map(&:contents).join("\n\n")
end
