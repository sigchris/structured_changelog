require 'structured_changelog'

desc "Show remaining roadmap items"
task "changelog:roadmap", [:repo_path, :changelog_path] do |_task, arguments|
  repo_path      = arguments.to_h.fetch(:repo_path)      { Pathname.pwd }
  changelog_path = arguments.to_h.fetch(:changelog_path) { Pathname.pwd/'CHANGELOG.md' }

  changelog = StructuredChangelog.new(changelog_path)

  changelog.roadmap_sections.each.with_index do |roadmap, index|
    puts unless index == 0

    puts roadmap
  end
end
