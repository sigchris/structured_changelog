require 'structured_changelog/core_ext/string'
require 'structured_changelog/core_ext/gem'

class StructuredChangelog
  class ReleasePreview
    def initialize(deps:)
      deps.verify_dependencies!(:changelog, :repo, :release_line_fetcher)

      @deps = deps
    end

    def to_s
      "## RELEASE #{new_version}\n\n#{release_lines}"
    end

    def empty?
      release_lines.empty?
    end

    def new_version
      @new_version ||= if release_lines.match?(/^*\ BREAKING:/)
                         current_version.bump_major
                       elsif release_lines.match?(/^*\ FEATURE:/)
                         current_version.bump_minor
                       elsif release_lines.match?(/^*\ FIX:/)
                         current_version.bump_patch
                       elsif release_lines.match?(/^*\ ENHANCEMENT:/)
                         current_version.bump_patch
                       elsif release_lines.match?(/^*\ DEPRECATION:/)
                         current_version.bump_patch
                       end
    end

    private

    attr_reader :deps

    def current_version
      @current_version ||= deps.changelog.version
    end

    def release_lines
      @release_lines ||= deps.release_line_fetcher
        .release_lines_since_tag("v#{current_version}")
        .join("\n")
    end
  end
end
