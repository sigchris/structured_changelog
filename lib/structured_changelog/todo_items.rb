Dir[File.join(File.dirname(__FILE__), "todo_items", "*.rb")].each(&method(:require))

class StructuredChangelog
  class InappropriateTodoItemComment < StandardError; end

  module TodoItems
    def self.todo_item_from_comment(comment)
      todo_item_class_for_comment = TodoItems::Base.subclasses.find do |subclass|
        subclass.appropriate_for_comment?(comment)
      end

      raise InappropriateTodoItemComment.new(comment) unless todo_item_class_for_comment

      todo_item_class_for_comment.from_comment(comment)
    end
  end
end
