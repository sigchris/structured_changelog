class StructuredChangelog
  class ReleaseLineFetcher
    RELEASE_LINE_PREFIXES = %w[
      BREAKING
      FEATURE
      FIX
      ENHANCEMENT
      DEPRECATION
    ]

    def initialize(deps:)
      deps.verify_dependencies!(:repo)

      @deps = deps
    end

    def release_lines_since_tag(tag)
      command = [
        "git log",
        # print the commit subject and body (taken together, these comprise a commit message)
        #  git pretty format docs: https://git-scm.com/docs/pretty-formats
        "--format=%B",
        # in naive chronological order
        "--reverse",
        # for commis committed after the tag
        "#{tag}..HEAD",
      ].join(" ")

      deps.repo
        .chdir { `#{command}` }
        .split("\n")
        .map(&:strip)
        .reject(&:empty?)
        .grep(/^\*\ (#{RELEASE_LINE_PREFIXES.join("|")})\:/)
    end

    private

    attr_reader :deps
  end
end
