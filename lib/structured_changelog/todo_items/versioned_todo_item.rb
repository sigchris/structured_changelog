require 'structured_changelog/todo_items/base'
require 'structured_changelog/patterns/roadmap_version'

class StructuredChangelog
  module TodoItems
    class VersionedTodoItem < Base
      def self.pattern
        /^#\s*#{Patterns::RoadmapVersion}\ TODO:/
      end

      def self.appropriate_for_comment?(comment)
        pattern =~ comment
      end

      def self.from_comment(comment)
        todo    = comment.gsub(pattern, '')
        version = Gem::Version.new(pattern.match(comment)[:version])

        new(todo: todo, version: version)
      end

      def initialize(todo:, version:)
        super(todo: todo)

        @version = version
      end

      def completion_version_from_version(_)
        version
      end

      private

      attr_reader :version
    end
  end
end
