class StructuredChangelog
  module TodoItems
    class Base
      def initialize(todo:)
        @todo = todo.strip
      end

      private

      attr_reader :todo
    end
  end
end
