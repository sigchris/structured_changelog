require 'structured_changelog/todo_items/base'

class StructuredChangelog
  module TodoItems
    class MinorTodoItem < Base
      def self.pattern
        /^#\s*MINOR\ TODO:/
      end

      def self.appropriate_for_comment?(comment)
        pattern =~ comment
      end

      def self.from_comment(comment)
        new(todo: comment.gsub(pattern, ''))
      end

      def completion_version_from_version(version)
        version.bump_minor
      end
    end
  end
end
