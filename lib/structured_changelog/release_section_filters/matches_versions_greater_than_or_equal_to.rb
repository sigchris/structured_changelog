require 'structured_changelog/release_section_filters/base'
require 'structured_changelog/version_pattern'

class StructuredChangelog
  module ReleaseSectionFilters
    class MatchesVersionsGreaterThanOrEqualTo < Base
      def self.pattern
        /^#{VersionPattern}\ \<$/
      end

      def filter_release_sections(release_sections)
        release_sections.select do |release_section| 
          Gem::Version.new(version) <= release_section.version
        end
      end

      private

      def version
        query.match(self.class.pattern)[:version]
      end
    end
  end
end
