require 'structured_changelog/release_section_filters/base'

class StructuredChangelog
  module ReleaseSectionFilters
    class MatchesCurrentVersion < Base
      def self.pattern
        /^current$/
      end

      def filter_release_sections(release_sections)
        [release_sections.max]
      end
    end
  end
end
