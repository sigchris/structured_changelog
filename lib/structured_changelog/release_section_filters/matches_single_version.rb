require 'structured_changelog/release_section_filters/base'
require 'structured_changelog/version_pattern'

class StructuredChangelog
  module ReleaseSectionFilters
    class MatchesSingleVersion < Base
      def self.pattern
        /^#{VersionPattern}$/
      end

      def filter_release_sections(release_sections)
        release_sections.select do |release_section|
          release_section.version == Gem::Version.new(query)
        end
      end
    end
  end
end
