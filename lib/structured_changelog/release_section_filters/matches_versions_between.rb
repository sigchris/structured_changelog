require 'structured_changelog/release_section_filters/base'
require 'structured_changelog/version_pattern'

class StructuredChangelog
  module ReleaseSectionFilters
    class MatchesVersionsBetween < Base
      def self.pattern
        /^(?<floor>#{VersionPattern})\ \<\ (?<ceiling>#{VersionPattern})$/
      end

      def filter_release_sections(release_sections)
        release_sections.select do |release_section|
          Gem::Version.new(floor) <= release_section.version &&
            release_section.version <= Gem::Version.new(ceiling)
        end
      end

      private

      def floor
        query.match(self.class.pattern)[:floor]
      end

      def ceiling
        query.match(self.class.pattern)[:ceiling]
      end
    end
  end
end
