require 'structured_changelog/release_section_filters/base'

class StructuredChangelog
  module ReleaseSectionFilters
    class MatchesAllVersions < Base
      def self.pattern
        /^all$/
      end

      def filter_release_sections(release_sections)
        release_sections
      end
    end
  end
end
