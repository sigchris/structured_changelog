class StructuredChangelog
  class Roadmap
    def initialize(deps:)
      deps.verify_dependencies!(:changelog)

      @deps = deps
    end

    def add_todo_item(todo_item)
      todo_completion_version = todo_item.completion_version_from_version(current_version)

      todo_item_lists_by_completion_version[todo_completion_version] << todo_item
    end

    def todo_item_lists_by_completion_version
      @todo_item_lists_by_completion_version ||= Hash.new do |h, version|
        h[version] = []
      end
    end

    private

    attr_reader :deps

    def current_version
      @current_version ||= deps.changelog.version
    end
  end
end
